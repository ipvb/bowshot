package bowshot

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestExpiredParse(t *testing.T) {

	sv := strings.Split("banjo.20190807-0808-59.log", ".")
	if len(sv) < 3 {
		// skip
		fmt.Fprintf(os.Stderr, "size too small %d\n", len(sv))
		return
	}
	filetime := sv[len(sv)-2]
	fmt.Fprintf(os.Stderr, "filetime: %s\n", filetime)
	if len(filetime) != len("20190101-0101-00") {
		return
	}
	y, err := strconv.Atoi(filetime[0:4])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Parse error: %v\n", err)
	}
	m, err := strconv.Atoi(filetime[4:6])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Parse error: %v\n", err)
	}
	d, err := strconv.Atoi(filetime[6:8])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Parse error: %v\n", err)
	}
	h, err := strconv.Atoi(filetime[9:11])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Parse error: %v\n", err)
	}
	min, err := strconv.Atoi(filetime[11:13])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Parse error: %v\n", err)
	}
	s, err := strconv.Atoi(filetime[14:16])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Parse error: %v\n", err)
	}
	ft := time.Date(y, time.Month(m), d, h, min, s, 0, time.Now().Location())
	fmt.Fprintf(os.Stderr, "%v\n", ft)
	// 422
}

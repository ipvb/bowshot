package bowshot

import (
	"fmt"
	"os"
	"strings"
	"time"
)

// Output logger
func (l *Slot) outputInternal(flush bool, prefix string, s string) error {
	if l.log != nil {
		now := time.Now()
		return l.log.writetimev(flush, prefix, s, now)
	}
	return nil
}

// DEBUG logger out
func (l *Slot) DEBUG(format string, v ...interface{}) {
	if l.level <= DebugLevel {
		_ = l.outputInternal(false, "[DEBUG] ", fmt.Sprintf(format, v...))
	}
}

// INFO logger out
func (l *Slot) INFO(format string, v ...interface{}) {
	_ = l.outputInternal(false, "[INFO] ", fmt.Sprintf(format, v...))
}

// STATUS output status
func (l *Slot) STATUS(format string, v ...interface{}) {
	msg := fmt.Sprintf(format, v...)
	if strings.HasSuffix(msg, "\n") {
		msg = msg[:len(msg)-1]
	}
	fmt.Fprintln(os.Stderr, msg)
	_ = l.outputInternal(true, "[INFO] ", msg)
}

// ERROR logger out
func (l *Slot) ERROR(format string, v ...interface{}) {
	_ = l.outputInternal(true, "[ERROR] ", fmt.Sprintf(format, v...))
}

// FATAL logger out
func (l *Slot) FATAL(format string, v ...interface{}) {
	_ = l.outputInternal(true, "[FATAL] ", fmt.Sprintf(format, v...))
}

// Fatal like log.Fatal
func (l *Slot) Fatal(v ...interface{}) {
	_ = l.outputInternal(true, "[EXIT] ", fmt.Sprint(v...))
	os.Exit(1)
}

// Access logger out like nginx
func (l *Slot) Access(format string, v ...interface{}) {
	if l.bus != nil {
		now := time.Now()
		_ = l.bus.writevaccess(fmt.Sprintf(format, v...), now)
	}
}

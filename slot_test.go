package bowshot

import (
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"sync"
	"testing"
	"time"
)

func TestCleanName(t *testing.T) {
	ss := []string{"ssss-dedew", "debug.log", "helloworld", "dddd(1)", "ujm+12.txt", "ded dede"}
	for _, s := range ss {
		fmt.Fprintf(os.Stderr, "CleanName: %s -->[%s]\n", s, cleanName(s))
	}
}

func TestDebug(t *testing.T) {
	if IsDebugEnable("Jack") {
		fmt.Fprintf(os.Stderr, "DEBUG\n")
	} else {
		fmt.Fprintf(os.Stderr, "NOT DEBUG\n")
	}
	os.Setenv("JACK_TRACE", "1")
	if IsDebugEnable("Jack") {
		fmt.Fprintf(os.Stderr, "DEBUG\n")
	} else {
		fmt.Fprintf(os.Stderr, "NOT DEBUG\n")
	}
	os.Setenv("JACK_TRACE", "TRUE")
	if IsDebugEnable("Jack") {
		fmt.Fprintf(os.Stderr, "DEBUG\n")
	} else {
		fmt.Fprintf(os.Stderr, "NOT DEBUG\n")
	}
	os.Setenv("JACK_TRACE", "false")
	if IsDebugEnable("Jack") {
		fmt.Fprintf(os.Stderr, "DEBUG\n")
	} else {
		fmt.Fprintf(os.Stderr, "NOT DEBUG\n")
	}
}

func TestParseSize(t *testing.T) {
	ss := []string{"1GB", "2GB", "100M", "1024k", "1024", "1024kb", "1mb"}
	for _, s := range ss {
		fmt.Fprintf(os.Stderr, "Filesize: %d\n", ParseMaximumSize(s))
	}
}

func TestDebugSlot(t *testing.T) {
	var slot Slot
	os.Setenv("BOWSHOT_TRACE", "1")
	_ = slot.InitializeEx("/tmp/slotdebug.access.log", "/tmp/slotdebug.error.log", DefaultSizeSettings())
	for i := 0; i < 500000; i++ {
		slot.INFO("%s", os.Args[0])
		slot.DEBUG("%s", os.Args[0])
	}
}

func TestSlot(t *testing.T) {
	var slot Slot
	_ = slot.InitializeEx("/tmp/slot.access.log", "/tmp/slot.error.log", DefaultSizeSettings())
	for i := 0; i < 1000000; i++ {
		slot.INFO("%s", os.Args[0])
		slot.DEBUG("%s", os.Args[0])
	}
}

func TestMultiSlot(t *testing.T) {
	var slot Slot
	_ = slot.InitializeEx("/tmp/slot.access.log", "/tmp/slot.error.log", DefaultSizeSettings())
	var wg sync.WaitGroup
	wg.Add(10)
	for i := 0; i < 10; i++ {
		go func() {
			for i := 0; i < 100000; i++ {
				slot.INFO("%s", os.Args[0])
				slot.DEBUG("%s", os.Args[0])
			}
			wg.Done()
		}()
	}
	wg.Wait()
}

func TestPlaceholder(t *testing.T) {
	///
	log.Printf("sss\n")
}

func TestSlotEx(t *testing.T) {
	f, err := os.Create(fmt.Sprintf("/tmp/cpu_%d_%s.prof", os.Getpid(), time.Now().Format("2006_01_02_03_04_05")))
	if err != nil {
		return
	}
	var slot Slot
	_ = pprof.StartCPUProfile(f)
	defer pprof.StopCPUProfile()
	_ = slot.InitializeEx("/tmp/slot_access.log", "/tmp/slot_error.log", DefaultSizeSettings())

	var wg sync.WaitGroup
	wg.Add(10)
	for j := 0; j < 10; j++ {
		go func() {
			for i := 0; i < 100000; i++ {
				slot.INFO("test %s %d", os.Args[0], i)
				slot.Access("POST /api/v3/internal/test %d", i)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}
